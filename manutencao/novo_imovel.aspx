﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="novo_imovel.aspx.cs" Inherits="manutencao_objectivo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SubTitleContent" Runat="Server">
    <asp:Literal ID="header_title" runat="server" Text="Inserir Novo Imóvel"></asp:Literal>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">

    <table>
        <tr>
            <td>
                <asp:Literal ID="Literal1" runat="server" Text="Tipologia:"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="ddlTipologia" runat="server" DataSourceID="SqlDataSourceTipologia"
                    DataTextField="nome" DataValueField="tipologiaId" CssClass="edPesq">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Literal ID="Literal2" runat="server" Text="Objetivo:"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="ddlObjetivo" runat="server" DataSourceID="SqlDataSourceObjetivo"
                    DataTextField="nome" DataValueField="objectivoId" CssClass="edPesq">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Literal ID="Literal3" runat="server" Text="Estado:"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="ddlEstado" runat="server" DataSourceID="SqlDataSourceEstado"
                    DataTextField="nome" DataValueField="EstadoId" CssClass="edPesq">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="Literal4" runat="server" Text="Concelho:"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="ddlConcelho" runat="server" DataSourceID="SqlDataSourceConcelho"
                    DataTextField="nome" DataValueField="ConcelhoId" AutoPostBack="true" CssClass="edPesq">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Literal ID="Literal5" runat="server" Text="Freguesia:"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="ddlFreguesia" runat="server" DataSourceID="SqlDataSourceFreguesia"
                    DataTextField="nome" DataValueField="freguesiaId" CssClass="edPesq">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Literal ID="Literal8" runat="server" Text="Tipo:"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="ddlTipo" runat="server" DataSourceID="SqlDataSourceTipo" DataTextField="nome"
                    DataValueField="TipoId" CssClass="edPesq">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="Literal7" runat="server" Text="Título:"></asp:Literal>
            </td>
            <td colspan="5">
                <asp:TextBox ID="txtTitulo" runat="server" CssClass="edPesq" Text="" 
                    Width="355px"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td><asp:Literal ID="Literal9" runat="server" Text="Área:"></asp:Literal></td>
        <td><asp:TextBox ID="edArea" runat="server" CssClass="edPesq" Text="0"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                                    ControlToValidate="txtPrecoDe" ErrorMessage="Só números!" MinimumValue="0" MaximumValue="400000000"
                                    SetFocusOnError="True" CssClass="validator" Type="Double"></asp:RangeValidator></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="Literal6" runat="server" Text="Preço:"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtPrecoDe" runat="server" CssClass="edPesq" Text="0"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                    ControlToValidate="txtPrecoDe" ErrorMessage="Só números!" MinimumValue="0" MaximumValue="400000000"
                                    SetFocusOnError="True" CssClass="validator" Type="Double"></asp:RangeValidator>
            </td>
            <td>
                <asp:Button ID="btSugerirValor" runat="server" onclick="btSugerirValor_Click" 
                    Text="Sugerir valor" />
            </td>
            <td>
                <asp:Label ID="lblValor" runat="server" Text="Valor: 0,00"></asp:Label>
            </td>
            <td>
            </td>
            <td>
                                
            </td>
        </tr>
        <tr><td colspan="6" align="right">
            <asp:Button ID="btApagar" runat="server" Text="Apagar" 
                    onclick="btApagar_Click" />
            <asp:Button ID="btGravar" runat="server" Text="Gravar" 
                    onclick="btGravar_Click" /></td></tr>
                        
    </table>

    <asp:SqlDataSource ID="SqlDataSourceImobi" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        
        SelectCommand='SELECT * FROM [imovel] WHERE ([imovelId] = @imovelId)'
        OnSelecting="SqlDataSourceImobi_Selecting" 
        DeleteCommand="DELETE FROM [imovel] WHERE [imovelId] = @imovelId" 
        InsertCommand="INSERT INTO [imovel] ([titulo], [ref], [tipoId], [tipologiaId], [objectivoId], [estadoId], [distritoId], [concelhoId], [freguesiaId], [zona], [preco], [areaUtil], [areaBruta], [areaTerreno], [descricao], [nquartos], [nwcs], [anoConstrucao], [morada], [codPostal], [localidade]) VALUES (@titulo, @ref, @tipoId, @tipologiaId, @objectivoId, @estadoId, @distritoId, @concelhoId, @freguesiaId, @zona, @preco, @areaUtil, @areaBruta, @areaTerreno, @descricao, @nquartos, @nwcs, @anoConstrucao, @morada, @codPostal, @localidade)" 
        
        UpdateCommand="UPDATE [imovel] SET [titulo] = @titulo, [ref] = @ref, [tipoId] = @tipoId, [tipologiaId] = @tipologiaId, [objectivoId] = @objectivoId, [estadoId] = @estadoId, [distritoId] = @distritoId, [concelhoId] = @concelhoId, [freguesiaId] = @freguesiaId, [zona] = @zona, [preco] = @preco, [areaUtil] = @areaUtil, [areaBruta] = @areaBruta, [areaTerreno] = @areaTerreno, [descricao] = @descricao, [nquartos] = @nquartos, [nwcs] = @nwcs, [anoConstrucao] = @anoConstrucao, [morada] = @morada, [codPostal] = @codPostal, [localidade] = @localidade WHERE [imovelId] = @imovelId">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="imovelId" 
                QueryStringField="id" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="imovelId" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="titulo" Type="String" />
            <asp:Parameter Name="ref" Type="String" />
            <asp:Parameter Name="tipoId" Type="Int32" />
            <asp:Parameter Name="tipologiaId" Type="Int32" />
            <asp:Parameter Name="objectivoId" Type="Int32" />
            <asp:Parameter Name="estadoId" Type="Int32" />
            <asp:Parameter Name="distritoId" Type="Int32" />
            <asp:Parameter Name="concelhoId" Type="Int32" />
            <asp:Parameter Name="freguesiaId" Type="Int32" />
            <asp:Parameter Name="zona" Type="String" />
            <asp:Parameter Name="preco" Type="Int32" />
            <asp:Parameter Name="areaUtil" Type="Double" />
            <asp:Parameter Name="areaBruta" Type="Double" />
            <asp:Parameter Name="areaTerreno" Type="Double" />
            <asp:Parameter Name="descricao" Type="String" />
            <asp:Parameter Name="nquartos" Type="Int32" />
            <asp:Parameter Name="nwcs" Type="Int32" />
            <asp:Parameter Name="anoConstrucao" Type="Int32" />
            <asp:Parameter Name="morada" Type="String" />
            <asp:Parameter Name="codPostal" Type="String" />
            <asp:Parameter Name="localidade" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="titulo" Type="String" />
            <asp:Parameter Name="ref" Type="String" />
            <asp:Parameter Name="tipoId" Type="Int32" />
            <asp:Parameter Name="tipologiaId" Type="Int32" />
            <asp:Parameter Name="objectivoId" Type="Int32" />
            <asp:Parameter Name="estadoId" Type="Int32" />
            <asp:Parameter Name="distritoId" Type="Int32" />
            <asp:Parameter Name="concelhoId" Type="Int32" />
            <asp:Parameter Name="freguesiaId" Type="Int32" />
            <asp:Parameter Name="zona" Type="String" />
            <asp:Parameter Name="preco" Type="Int32" />
            <asp:Parameter Name="areaUtil" Type="Double" />
            <asp:Parameter Name="areaBruta" Type="Double" />
            <asp:Parameter Name="areaTerreno" Type="Double" />
            <asp:Parameter Name="descricao" Type="String" />
            <asp:Parameter Name="nquartos" Type="Int32" />
            <asp:Parameter Name="nwcs" Type="Int32" />
            <asp:Parameter Name="anoConstrucao" Type="Int32" />
            <asp:Parameter Name="morada" Type="String" />
            <asp:Parameter Name="codPostal" Type="String" />
            <asp:Parameter Name="localidade" Type="String" />
            <asp:Parameter Name="imovelId" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceTipologia" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [tipologia] WHERE nome !='' order by nome"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceObjetivo" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [objectivo] where nome !=''"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceEstado" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [estado] where nome !=''"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceConcelho" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [concelho]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceFreguesia" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [freguesia] WHERE ([concelhoId] = @concelhoId)"
        OnSelecting="SqlDataSourceFreguesia_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlConcelho" Name="concelhoId" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceTipo" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [tipo]"></asp:SqlDataSource>






    <asp:SqlDataSource ID="SqlDataSourceCalc" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand='SELECT dbo.ImobiSemePreco(@EstadoId,imovel.EstadoId,@EstadoPeso,@ConcelhoId,@FreguesiaId,imovel.concelhoId,imovel.FreguesiaId,@FreguesiaPeso,@TipologiaId,imovel.tipologiaId,@TipologiaPeso,@TipoId,imovel.tipoId,@TipoPeso,@Objectivo,imovel.ObjectivoId,@ObjectivoPeso,@Area,imovel.areaUtil,@AreaPeso) as Seme ,imovel.imovelId, imovel.titulo, imovel.ref, imovel.localidade, imovel.codPostal, imovel.morada, imovel.anoConstrucao, imovel.nwcs, imovel.nquartos, freguesia.nome as freguesia, tipologia.nome AS tipologia, imovel.descricao, imovel.areaBruta, imovel.areaTerreno, imovel.zona, imovel.preco, imovel.areaUtil, distrito.nome AS distrito, estado.nome AS estado, objectivo.nome AS objectivo, tipo.nome AS tipo, concelho.nome AS concelho FROM concelho INNER JOIN imovel ON concelho.concelhoId = imovel.concelhoId INNER JOIN objectivo ON imovel.objectivoId = objectivo.objectivoId INNER JOIN tipo ON imovel.tipoId = tipo.tipoId INNER JOIN tipologia ON imovel.tipologiaId = tipologia.tipologiaId INNER JOIN estado ON imovel.estadoId = estado.estadoId INNER JOIN freguesia ON imovel.concelhoId = freguesia.concelhoId AND imovel.freguesiaId = freguesia.freguesiaId CROSS JOIN distrito where  preco>0   ORDER BY dbo.ImobiSemePreco(@EstadoId,imovel.EstadoId,@EstadoPeso,@ConcelhoId,@FreguesiaId,imovel.concelhoId,imovel.FreguesiaId,@FreguesiaPeso,@TipologiaId,imovel.tipologiaId,@TipologiaPeso,@TipoId,imovel.tipoId,@TipoPeso,@Objectivo,imovel.ObjectivoId,@ObjectivoPeso,@Area,imovel.areaUtil,@AreaPeso) desc, preco asc'
        OnSelecting="SqlDataSourceImobi_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlEstado" Name="EstadoId" PropertyName="SelectedValue" />
            <asp:Parameter DefaultValue="1" Name="EstadoPeso" />
            <asp:ControlParameter ControlID="ddlConcelho" Name="ConcelhoId" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="ddlFreguesia" Name="FreguesiaId" PropertyName="SelectedValue" />
            <asp:Parameter DefaultValue="1" Name="FreguesiaPeso" />
            <asp:ControlParameter ControlID="ddlTipologia" Name="TipologiaId" PropertyName="SelectedValue" />
            <asp:Parameter DefaultValue="1" Name="TipologiaPeso" />
            <asp:ControlParameter ControlID="ddlTipo" Name="TipoId" PropertyName="SelectedValue" />
            <asp:Parameter DefaultValue="1" Name="TipoPeso" />
            <asp:ControlParameter ControlID="ddlObjetivo" Name="Objectivo" PropertyName="SelectedValue" />
            <asp:Parameter DefaultValue="1" Name="ObjectivoPeso" />
            <asp:ControlParameter ControlID="edArea" DefaultValue="" Name="Area" 
                PropertyName="Text" />
            <asp:Parameter DefaultValue="1" Name="AreaPeso" />
        </SelectParameters>
    </asp:SqlDataSource>


</asp:Content>

