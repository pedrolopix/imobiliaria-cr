﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="tipo.aspx.cs" Inherits="manutencao_tipo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

<script type="text/javascript">
    function clearDialog() {
        fillDialog(0, "");
    }
    function fillDialog(ID, Nome) {
        $("#txttipoId").val(ID);
        $("#txtNome").val(Nome);

    }
    function dialogEditShow(ID, Nome) {
        fillDialog(ID, Nome);
        $("#dialog:ui-dialog").dialog("destroy");
        $("#dialog-modal").dialog({
            title: "Editar tipo",
            height: 280,
            width: 400,
            modal: true,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }


        });

    };
    function dialogAddShow() {
        clearDialog();
        $("#dialog:ui-dialog").dialog("destroy");
        $("#dialog-modal").dialog({
            title: "Adicionar tipo",
            height: 280,
            width: 400,
            modal: true,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });

    };
    function confirmDelete(id, Nome) {
        $("#txtIDDel").val(id);
        $("#dialog:ui-dialog").dialog("destroy");
        $("#dialog-cofirm").dialog({
            title: Nome,
            height: 150,
            width: 400,
            modal: true,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    };


</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SubTitleContent" Runat="Server">
    Edição de tipos  
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">

    <div style="width:100%; text-align:center;">
        <asp:Label ID="lblError" runat="server" Text="" CssClass="failureNotification"></asp:Label>
    </div>
    <table width="100%">
        <tr>
            <td valign="middle" style="width:22px;">
                <a href="#" onclick="show()"><asp:Image ID="Image1" runat="server" ImageUrl="~/Styles/img/add.png" /></a>
            </td>
            <td style="width:150px;">
                <a href="#" onclick="dialogAddShow()">Adicionar</a>
            </td>
            <td style="width:40%;">
            </td>
            <td style="text-align:right;">
                <asp:TextBox ID="txtPesquisa" runat="server" Text="" ToolTip="Pesquisar..."></asp:TextBox>
                <asp:ImageButton ID="imgBtPesquisa" runat="server" 
                    ImageUrl="~/Styles/img/search.png" onclick="imgBtPesquisa_Click" />
            </td>
        </tr>
    </table>
        <asp:ListView ID="lstDados" runat="server" DataSourceID="sql" DataKeyNames="tipoId">
       
             <LayoutTemplate>
          
                <table cellpadding="2" width="100%" border="0" id="tblDados" class="dados">
                  <tr id="Tr1" runat="server">
                       <th id="Th2" runat="server" style="width:23px; text-align:center"></th>
                       <th id="Th1" runat="server" style="width:auto;"><asp:LinkButton ID="lnkNome" runat="server" CommandName="Sort" CommandArgument="Nome">Nome</asp:LinkButton></th>
                        
                  </tr>
                  <tr runat="server" id="itemPlaceholder" />
                </table>
               
                <br />
                 Página: <asp:DataPager ID="DataPager1" runat="server" PageSize="15">
                 <Fields>
            <asp:NumericPagerField ButtonCount="10" PreviousPageText="<" NextPageText=">" />
            </Fields>
            </asp:DataPager>
            </LayoutTemplate>
            <ItemTemplate >
            
            <tr id="Tr1" runat="server" style="background-color:White;" class='<%# Container.DisplayIndex % 2 == 0 ? "" : "a" %>'>
              <td id="Td5" runat="server">
              
              <a href="#" onclick='confirmDelete(<%#Eval("tipoId")%>,"<%#Eval("Nome") %>")'>
               <asp:Image ID="Image2" runat="server" ImageUrl="~/Styles/img/remove.png"/></a>
                
              </td>
              <td id="Td1" runat="server"> 
                <a href="#" onclick='dialogEditShow(<%#Eval("tipoId")%>,"<%#Eval("nome") %>")'>
                <%# Highlight(txtPesquisa.Text, Eval("Nome").ToString()) %>
                </a>
              </td>
              
              
              
            </tr>
           
          </ItemTemplate>
          <EmptyDataTemplate>Não existem tipos.</EmptyDataTemplate>
        </asp:ListView>
        <div style="text-align:center">
    </div>
   
    <div  id="dialog-modal" style="display:none;" title="Adicionar Tipo  Ementa">
        <table>
            <tr>
                <td>
                    Nome :
                </td>
                <td>
                    <asp:HiddenField ID="txttipoId" runat="server" ClientIDMode="Static"/>
                    <asp:TextBox ID="txtNome" runat="server" CssClass="text ui-widget-content ui-corner-all" Width="250px" ClientIDMode="Static"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="txtNome" ID="rfvNome" runat="server" Text="*" ErrorMessage="Nome é obrigatório!" SetFocusOnError="true" ValidationGroup="Dados"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:Button ID="btnGravar" runat="server" Text="Gravar" onclick="btnGravar_Click" ValidationGroup="Dados" />
                    <asp:Button ID="Button2" runat="server" Text="Cancelar" CausesValidation="false" />
                 </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                    HeaderText="" DisplayMode="BulletList" ValidationGroup="Dados" CssClass="showErrorSummary" />
                </td>
            </tr>

            </table>
    </div>
    

    <div id="dialog-cofirm" title="Confirmação" style="display:none;">
    <table>
    <tr align="center">
    <td colspan="2">
        <asp:HiddenField ID="txtIDDel" runat="server" ClientIDMode="Static"/>
        Deseja apagar o registo selecionado ?
    </td>
    </tr>
    <tr align="center">
    <td><asp:Button ID="btSim" runat="server" Text="Sim" onclick="btSim_Click" /></td>
    <td><asp:Button ID="btNao" runat="server" Text="Não" CausesValidation="false" />
     </td>
    </tr>
        </table>
    </div>


    <asp:SqlDataSource ID="sql" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
        SelectCommand="SELECT * FROM [tipo] where nome LIKE '%'+@pesq+'%' ORDER BY nome"
        DeleteCommand="DELETE FROM [tipo] WHERE [tipoId] = @tipoId" 
        InsertCommand="INSERT INTO [tipo] ([nome]) VALUES (@nome)" 
        
        
        UpdateCommand="UPDATE [tipo] SET [nome] = @nome WHERE [tipoId] = @tipoId" 
        ondeleted="sql_Deleted" onselecting="sql_Selecting">
        <SelectParameters>
           <asp:ControlParameter ControlID="txtPesquisa" DefaultValue="%" Name="pesq" PropertyName="Text" Type="String" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="tipoId" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="tipoId" Type="Int32" />
            <asp:Parameter Name="nome" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="nome" Type="String" />
            <asp:Parameter Name="tipoId" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

