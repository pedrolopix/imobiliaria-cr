﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class manutencao_objectivo : System.Web.UI.Page
{
    private int id;
    protected void Page_Load(object sender, EventArgs e)
    {

        string ids = Request.QueryString["ID"];
        id = 0;
        btApagar.Visible = false;
        if (!String.IsNullOrEmpty(ids))
        {
            try
            {
                id = Convert.ToInt32(ids);
            }
            catch (Exception)
            {
                id = 0;
            }

            if (!IsPostBack && id!=0)
            {
                header_title.Text = "Editar Imóvel";
                btApagar.Visible = true;
                fill();
            }
        }
    }

    private void fill()
    {
        DataView dview = (DataView)SqlDataSourceImobi.Select(DataSourceSelectArguments.Empty);
        foreach (DataRow drow in dview.Table.Rows)
        {
            ddlConcelho.SelectedValue = drow["concelhoId"].ToString();
            ddlFreguesia.SelectedValue = drow["freguesiaId"].ToString();
            ddlEstado.SelectedValue = drow["estadoId"].ToString();
            ddlObjetivo.SelectedValue = drow["objectivoId"].ToString();
            ddlTipo.SelectedValue = drow["tipoId"].ToString();
            ddlTipologia.SelectedValue = drow["tipologiaId"].ToString();
            txtPrecoDe.Text = drow["preco"].ToString();
            txtTitulo.Text = drow["titulo"].ToString();
            return;
        }
    }

    protected void SqlDataSourceFreguesia_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
    }

    protected void SqlDataSourceImobi_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }

    protected void btGravar_Click(object sender, EventArgs e)
    {
        if (id == 0)
        {
            insert();
        }
        else
        {
            update();
        }

        Response.Redirect("~/Default.aspx");
    }

    private void update()
    {
        SqlDataSourceImobi.UpdateParameters["imovelId"].DefaultValue = id.ToString();
        SqlDataSourceImobi.UpdateParameters["freguesiaId"].DefaultValue = ddlFreguesia.SelectedValue;
        SqlDataSourceImobi.UpdateParameters["tipoId"].DefaultValue = ddlTipo.SelectedValue;
        SqlDataSourceImobi.UpdateParameters["tipologiaId"].DefaultValue = ddlTipologia.SelectedValue;
        SqlDataSourceImobi.UpdateParameters["objectivoId"].DefaultValue = ddlObjetivo.SelectedValue;
        SqlDataSourceImobi.UpdateParameters["estadoId"].DefaultValue = ddlEstado.SelectedValue;
        SqlDataSourceImobi.UpdateParameters["concelhoId"].DefaultValue = ddlConcelho.SelectedValue;
        SqlDataSourceImobi.UpdateParameters["preco"].DefaultValue = txtPrecoDe.Text;
        SqlDataSourceImobi.UpdateParameters["titulo"].DefaultValue = txtTitulo.Text;
        SqlDataSourceImobi.Update();
    }

    private void insert()
    {
        SqlDataSourceImobi.InsertParameters["freguesiaId"].DefaultValue = ddlFreguesia.SelectedValue;
        SqlDataSourceImobi.InsertParameters["tipoId"].DefaultValue = ddlTipo.SelectedValue;
        SqlDataSourceImobi.InsertParameters["tipologiaId"].DefaultValue = ddlTipologia.SelectedValue;
        SqlDataSourceImobi.InsertParameters["objectivoId"].DefaultValue = ddlObjetivo.SelectedValue;
        SqlDataSourceImobi.InsertParameters["estadoId"].DefaultValue = ddlEstado.SelectedValue;
        SqlDataSourceImobi.InsertParameters["concelhoId"].DefaultValue = ddlConcelho.SelectedValue;
        SqlDataSourceImobi.InsertParameters["preco"].DefaultValue = txtPrecoDe.Text;
        SqlDataSourceImobi.InsertParameters["titulo"].DefaultValue = txtTitulo.Text;
        SqlDataSourceImobi.Insert();
    }

    protected void btApagar_Click(object sender, EventArgs e)
    {
        SqlDataSourceImobi.DeleteParameters["imovelId"].DefaultValue = id.ToString();
        SqlDataSourceImobi.Delete();
        Response.Redirect("~/Default.aspx");
    }


    protected void btSugerirValor_Click(object sender, EventArgs e)
    {
        string cnnString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        SqlConnection connection = new SqlConnection(cnnString);
        connection.Open();
        Config cfg = new Config(connection);

        SqlDataSourceCalc.SelectParameters["EstadoPeso"].DefaultValue = cfg.pesoEstado.ToString();
        SqlDataSourceCalc.SelectParameters["FreguesiaPeso"].DefaultValue = cfg.pesoFreguesia.ToString();
        SqlDataSourceCalc.SelectParameters["TipoPeso"].DefaultValue = cfg.pesoTipo.ToString();
        SqlDataSourceCalc.SelectParameters["TipologiaPeso"].DefaultValue = cfg.pesoTipologia.ToString();
       // SqlDataSourceCalc.SelectParameters["PrecoPeso"].DefaultValue = cfg.pesoPreco.ToString();
        SqlDataSourceCalc.SelectParameters["ObjectivoPeso"].DefaultValue = cfg.pesoObjectivo.ToString();
        SqlDataSourceCalc.SelectParameters["AreaPeso"].DefaultValue = cfg.pesoArea.ToString();
        
        DataView dview = (DataView)SqlDataSourceCalc.Select(DataSourceSelectArguments.Empty);
        foreach (DataRow row in dview.Table.Rows)
        {
            double preco=0;
            double semelhanca=0;
            try
            {
                preco = Convert.ToDouble(row["preco"].ToString());
                semelhanca = Convert.ToDouble(row["Seme"].ToString());
                double area = Convert.ToDouble(row["areaUtil"].ToString());
                double c = preco + (1 - semelhanca) * preco;

                lblValor.Text = String.Format("preço : {0:C}, area {3:N}, semelhança: {1:N} </br><b>valor sugerido: {2:C}</b>", preco,  semelhanca, c, area);
                
            }
            catch (Exception)
            {
                preco = 0;
                
            }
            return;
        }
    }
}