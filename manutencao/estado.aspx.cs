﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class manutencao_estado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
    }

    protected void limpar()
    {
        txtestadoId.Value = "0";
        txtNome.Text = "";
    }

    protected void gravar()
    {
        int id = Convert.ToInt32(txtestadoId.Value);
        if (id == 0)
        {
            sql.InsertParameters["Nome"].DefaultValue = txtNome.Text;
            sql.Insert();
        }
        else
        {
            sql.UpdateParameters["estadoId"].DefaultValue = id.ToString();
            sql.UpdateParameters["Nome"].DefaultValue = txtNome.Text;
            sql.Update();
        }
        limpar();
        Response.Write("<script>location.href='estado.aspx'</script>");
    }

    protected void btnGravar_Click(object sender, EventArgs e)
    {
        gravar();
    }

    protected void imgBtPesquisa_Click(object sender, ImageClickEventArgs e)
    {
        lstDados.DataBind();
    }

    public string Highlight(string Search_Str, string InputTxt)
    {
        Regex RegExp = new Regex(Search_Str.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
        return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
    }

    public string ReplaceKeyWords(Match m)
    {
        return "<span class=highlight>" + m.Value + "</span>";
    }

    protected void btSim_Click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(txtIDDel.Value);
        if (id != 0)
        {
            sql.DeleteParameters["estadoId"].DefaultValue = id.ToString();
            sql.Delete();
        }
    }
    protected void sql_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception != null)
        {
            lblError.Text = "Não foi possivel eliminar o registo.";
            e.ExceptionHandled = true;
        }
    }
    protected void sql_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
}