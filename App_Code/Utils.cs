﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Utils
/// </summary>
public class Utils
{
	public Utils()
	{
		
	}

    public static RangeValidator RangeValidatorLE1(string controlToValidate)
    {

        RangeValidator rv = new RangeValidator();
        rv.ControlToValidate = controlToValidate;
        rv.Type = ValidationDataType.Double;
        rv.Display = ValidatorDisplay.Dynamic;
        rv.Text = "*";
        rv.CssClass = "validator";
        rv.SetFocusOnError = true;
        rv.MaximumValue = "1";
        rv.MinimumValue = "0";
        rv.ErrorMessage = "valores entre 0 e 1, ex: 0,111";
        return rv;
    }

    public static System.Web.UI.Control RangeValidatorLE1(TextBox tb)
    {
        return RangeValidatorLE1(tb.ClientID.ToString());
    }
}