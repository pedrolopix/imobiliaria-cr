﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.XPath;

/// <summary>
/// Summary description for GoogleMapsApi
/// </summary>
public class GoogleMapsApi
{
    public GoogleMapsApi()
    {

    }

    public int getDistancia(string deFreguesia, string deConcelho, string paraFreguesia, string paraConcelho)
    {
        //System.Threading.Thread.Sleep(1000);
        string url = @"http://maps.googleapis.com/maps/api/distancematrix/xml?origins={0}+{1}+Portugal&destinations={2}+{3}+Portugal&sensor=false";
        url = string.Format(url, deFreguesia, deConcelho, paraFreguesia, paraConcelho);
        WebRequest req = HttpWebRequest.Create(url);
        req.Method = "GET";

        //string source;
        using (StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream()))
        {
            //    source = reader.ReadToEnd();
            //}

            XPathDocument doc = new XPathDocument(reader);
            XPathNavigator nav = doc.CreateNavigator();

            XPathExpression expr;
            expr = nav.Compile("/DistanceMatrixResponse/row/element/distance/value");
            XPathNodeIterator iterator = nav.Select(expr);
            while (iterator.MoveNext())
            {
                XPathNavigator nav2 = iterator.Current.Clone();
                return Convert.ToInt32(nav2.Value);
                
            }
            
        }

        return 0;

        //XmlTextReader xml = new XmlTextReader(url);
        //xml.Read();

        //XmlDocument doc = new XmlDocument();
        //doc.LoadXml(source);
        //XmlNodeList list=doc.GetElementsByTagName("DistanceMatrixResponse");


    }

}
