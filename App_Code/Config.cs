﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Config
/// </summary>
public class Config
{
    private SqlConnection connection;
    public Config(SqlConnection connection)
    {
        this.connection = connection;
    }



    public string getValue(string key, string def)
    {
        string r = def;
        string sql = "select value from config where [key]=@key ";
        using (SqlCommand cmd = new SqlCommand(sql, connection))
        {
            cmd.Parameters.AddWithValue("@key", key);
            SqlDataReader myReader = cmd.ExecuteReader();
            if (myReader.Read())
            {
                r = myReader.GetString(0);
            }
            myReader.Close();
            return r.Trim();
        }
    }

    public int getValueAsInt(string key, int def)
    {
        string r = getValue(key, def.ToString());
        return Convert.ToInt32(r);
    }

    public void setValue(string key, string value)
    {
        string sql = "select  value from config where [key]=@key ";
        bool existe = false;
        using (SqlCommand cmd = new SqlCommand(sql, connection))
        {
            cmd.Parameters.AddWithValue("@key", key);
            SqlDataReader myReader = cmd.ExecuteReader();
            existe = (myReader.Read());
            myReader.Close();
        }
        if (existe)
        {
            sql = "update config set value=@value where [key]=@key";
        }
        else
        {
            sql = "INSERT INTO config([key], value) VALUES (@key,@value)";
        }
        using (SqlCommand cmd = new SqlCommand(sql, connection))
        {

            cmd.Parameters.AddWithValue("@key", key);
            cmd.Parameters.AddWithValue("@value", value);
            cmd.ExecuteNonQuery();
        }
    }

    public void setValue(string key, int value)
    {
        setValue(key, value.ToString());
    }

    public int pesoEstado
    {
        get
        {
            return getValueAsInt("pesp.estado", 1);
        }
        set
        {
            setValue("pesp.estado", value);
        }
    }

    public int pesoFreguesia
    {
        get
        {
            return getValueAsInt("pesp.freguesia", 1);
        }
        set
        {
            setValue("pesp.freguesia", value);
        }
    }

    public int pesoTipo
    {
        get
        {
            return getValueAsInt("pesp.tipo", 1);
        }
        set
        {
            setValue("pesp.tipo", value);
        }
    }

    public int pesoTipologia
    {
        get
        {
            return getValueAsInt("pesp.tipologia", 1);
        }
        set
        {
            setValue("pesp.tipologia", value);
        }
    }

    public int pesoPreco
    {
        get
        {
            return getValueAsInt("pesp.preco", 1);
        }
        set
        {
            setValue("pesp.preco", value);
        }
    }

    public int pesoObjectivo
    {
        get
        {
            return getValueAsInt("pesp.objectivo", 1);
        }
        set
        {
            setValue("pesp.objectivo", value);
        }
    }

    public int pesoArea
    {
        get
        {
            return getValueAsInt("pesp.area", 1);
        }
        set
        {
            setValue("pesp.area", value);
        }
    }



}