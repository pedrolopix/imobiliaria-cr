
SELECT    imovel.imovelId, imovel.titulo,imovel.ref, imovel.localidade, imovel.codPostal, 
		  imovel.morada, imovel.anoConstrucao, imovel.nwcs, imovel.nquartos,  freguesia.nome, tipologia.nome AS "tipologia", 
                      imovel.descricao, imovel.areaBruta, imovel.areaTerreno, imovel.zona, imovel.preco, imovel.areaUtil, distrito.nome AS "distrito", 
                      estado.nome AS "estado", objectivo.nome AS "objectivo", tipo.nome AS "tipo", 
                      concelho.nome AS "concelho"
FROM      concelho INNER JOIN
                      imovel ON concelho.concelhoId = imovel.concelhoId INNER JOIN
                      objectivo ON imovel.objectivoId = objectivo.objectivoId INNER JOIN
                      tipo ON imovel.tipoId = tipo.tipoId INNER JOIN
                      tipologia ON imovel.tipologiaId = tipologia.tipologiaId INNER JOIN
                      estado ON imovel.estadoId = estado.estadoId INNER JOIN
                      freguesia ON imovel.concelhoId = freguesia.concelhoId AND imovel.freguesiaId = freguesia.freguesiaId CROSS JOIN
                      distrito