﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;

public partial class admin_freguesia_dist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["fregcount"] = -1;
    }
    protected void btnCalcAll_Click(object sender, EventArgs e)
    {
        Thread oThread = new Thread(new ThreadStart(CalcDistFreguesias));
        oThread.Start();


    }

    private void CalcDistFreguesias()
    {

        GoogleMapsApi map = new GoogleMapsApi();
        Session["fregcount"] = 0;

        DataView dview = (DataView)SqlDataSourceFreguesia.Select(DataSourceSelectArguments.Empty);
        foreach (DataRow dcol in dview.Table.Rows)
        {
            foreach (DataRow drow in dview.Table.Rows)
            {
                int dist = 0;
                int c = (int)(Session["fregcount"]);
                c++;
                Session["fregcount"] = c;
                bool exist = false;
                dist = getDistancia(dcol["concelhoId"].ToString(), dcol["freguesiaId"].ToString(), drow["concelhoId"].ToString(), drow["freguesiaId"].ToString(), out exist);
                if (!exist && dist == 0)
                {
                    dist = map.getDistancia(dcol["nome"].ToString(), dcol["concelho"].ToString(), drow["nome"].ToString(), drow["concelho"].ToString());
                    setDistancia(dcol["concelhoId"].ToString(), dcol["freguesiaId"].ToString(), drow["concelhoId"].ToString(), drow["freguesiaId"].ToString(), dist);
                }  

            }

        }
        Session["fregcount"] = 0;
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        int c = (int)(Session["fregcount"]);
        if (c == -1)
        {
            lblinfo.Text = "";
        } else
        if (c == 0)
        {
            btnCalcAll.Enabled = true;
            lblinfo.Text = "Atualização concluida!";
        }
        else
        {
            btnCalcAll.Enabled = false;
            lblinfo.Text = "A atualizar... ";
            lblinfo.Text += c.ToString();
        }
    }

    private int getDistancia(string concelhoIdDe, string freguesiaIdDe, string concelhoIdPara, string freguesiaIdPara, out bool exist)
    {
        exist = false;
        SqlDataSourceDist.SelectParameters["freguesiaIdDe"].DefaultValue = freguesiaIdDe;
        SqlDataSourceDist.SelectParameters["concelhoIdDe"].DefaultValue = concelhoIdDe;
        SqlDataSourceDist.SelectParameters["freguesiaIdPara"].DefaultValue = freguesiaIdPara;
        SqlDataSourceDist.SelectParameters["concelhoIdPara"].DefaultValue = concelhoIdPara;

        DataView dview1 = (DataView)SqlDataSourceDist.Select(DataSourceSelectArguments.Empty);
        foreach (DataRow distrow in dview1.Table.Rows)
        {
            exist = true;
            return Convert.ToInt32(distrow["dist"].ToString());
        }
        return 0;

    }

    private void setDistancia(string concelhoIdDe, string freguesiaIdDe, string concelhoIdPara, string freguesiaIdPara, int dist)
    {
        SqlDataSourceDist.SelectParameters["freguesiaIdDe"].DefaultValue = freguesiaIdDe;
        SqlDataSourceDist.SelectParameters["concelhoIdDe"].DefaultValue = concelhoIdDe;
        SqlDataSourceDist.SelectParameters["freguesiaIdPara"].DefaultValue = freguesiaIdPara;
        SqlDataSourceDist.SelectParameters["concelhoIdPara"].DefaultValue = concelhoIdPara;

        DataView dview1 = (DataView)SqlDataSourceDist.Select(DataSourceSelectArguments.Empty);
        if (dview1.Table.Rows.Count == 0)
        {
            SqlDataSourceDist.InsertParameters["freguesiaIdDe"].DefaultValue = freguesiaIdDe;
            SqlDataSourceDist.InsertParameters["concelhoIdDe"].DefaultValue = concelhoIdDe;
            SqlDataSourceDist.InsertParameters["freguesiaIdPara"].DefaultValue = freguesiaIdPara;
            SqlDataSourceDist.InsertParameters["concelhoIdPara"].DefaultValue = concelhoIdPara;
            SqlDataSourceDist.InsertParameters["dist"].DefaultValue = dist.ToString();
            SqlDataSourceDist.Insert();
        }
        else
        {
            SqlDataSourceDist.UpdateParameters["freguesiaIdDe"].DefaultValue = freguesiaIdDe;
            SqlDataSourceDist.UpdateParameters["concelhoIdDe"].DefaultValue = concelhoIdDe;
            SqlDataSourceDist.UpdateParameters["freguesiaIdPara"].DefaultValue = freguesiaIdPara;
            SqlDataSourceDist.UpdateParameters["concelhoIdPara"].DefaultValue = concelhoIdPara;
            SqlDataSourceDist.UpdateParameters["dist"].DefaultValue = dist.ToString();
            SqlDataSourceDist.Update();
        }
        

    }



    protected void ddlConcelhoDe_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool exist;
        edDistancia.Text = getDistancia(ddlConcelhoDe.SelectedValue, ddlFregesiaDe.SelectedValue, ddlConcelhoPara.SelectedValue, ddlFregesiaPara.SelectedValue, out exist).ToString();
    }


    protected void btGravar_Click(object sender, EventArgs e)
    {
        setDistancia(ddlConcelhoDe.SelectedValue, ddlFregesiaDe.SelectedValue, ddlConcelhoPara.SelectedValue, ddlFregesiaPara.SelectedValue, Convert.ToInt32(edDistancia.Text));
    }
}