﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true"
    CodeFile="freguesia_dist.aspx.cs" Inherits="admin_freguesia_dist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubTitleContent" runat="Server">
    Distâncias entre freguesias
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Timer ID="UpdateTimer" runat="server" Interval="1000" OnTick="Timer1_Tick">
    </asp:Timer>

    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Concelho de:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlConcelhoDe" runat="server" DataSourceID="SqlDataSourceConcelho"
                    DataTextField="nome" DataValueField="concelhoId" AutoPostBack="True" 
                    CssClass="edPesq" 
                    onselectedindexchanged="ddlConcelhoDe_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Freguesia de:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlFregesiaDe" runat="server" DataSourceID="SqlDataSourceFreguesiaDe"
                    DataTextField="nome" DataValueField="FreguesiaId" AutoPostBack="True" 
                    CssClass="edPesq" onselectedindexchanged="ddlConcelhoDe_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Concelho para:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlConcelhoPara" runat="server" DataSourceID="SqlDataSourceConcelho"
                    DataTextField="nome" DataValueField="concelhoId" AutoPostBack="True" 
                    CssClass="edPesq" 
                    onselectedindexchanged="ddlConcelhoDe_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Freguesia para:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlFregesiaPara" runat="server" DataSourceID="SqlDataSourceFreguesiaPara"
                    DataTextField="nome" DataValueField="FreguesiaId" AutoPostBack="True" 
                    CssClass="edPesq" onselectedindexchanged="ddlConcelhoDe_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
        <td>
                <asp:Label ID="Label33" runat="server" Text="Distância (metros):"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="edDistancia" runat="server"></asp:TextBox>
                <asp:Button ID="btGravar" runat="server" Text="Gravar" 
                    onclick="btGravar_Click" />
            </td>
        <td colspan="2">
            
        </td>
        </tr>
    </table>
    </br>
    <asp:Panel ID="PanelCalcAll" runat="server">
    <asp:Button ID="btnCalcAll" runat="server" Text="Atualizar todas as distâncias" OnClick="btnCalcAll_Click" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
            </Triggers>
            <ContentTemplate>
                
                <asp:Label ID="lblinfo" runat="server"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>


    <asp:SqlDataSource ID="SqlDataSourceFreguesia" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT freguesia.concelhoId, freguesia.freguesiaId, freguesia.nome, concelho.nome AS Concelho FROM freguesia INNER JOIN concelho ON freguesia.concelhoId = concelho.concelhoId">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceDist" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        DeleteCommand="DELETE FROM [freguesiaDist] WHERE [freguesiaIdDe] = @freguesiaIdDe AND [concelhoIdDe] = @concelhoIdDe AND [freguesiaIdPara] = @freguesiaIdPara AND [concelhoIdPara] = @concelhoIdPara"
        InsertCommand="INSERT INTO [freguesiaDist] ([freguesiaIdDe], [concelhoIdDe], [freguesiaIdPara], [concelhoIdPara], [dist]) VALUES (@freguesiaIdDe, @concelhoIdDe, @freguesiaIdPara, @concelhoIdPara, @dist)"
        SelectCommand="SELECT freguesiaIdDe, concelhoIdDe, freguesiaIdPara, concelhoIdPara, dist FROM freguesiaDist WHERE (freguesiaIdDe = @freguesiaIdDe) AND (concelhoIdDe = @concelhoIdDe) AND (freguesiaIdPara = @freguesiaIdPara) AND (concelhoIdPara = @concelhoIdPara)"
        UpdateCommand="UPDATE [freguesiaDist] SET [dist] = @dist WHERE [freguesiaIdDe] = @freguesiaIdDe AND [concelhoIdDe] = @concelhoIdDe AND [freguesiaIdPara] = @freguesiaIdPara AND [concelhoIdPara] = @concelhoIdPara">
        <DeleteParameters>
            <asp:Parameter Name="freguesiaIdDe" Type="Int32" />
            <asp:Parameter Name="concelhoIdDe" Type="Int32" />
            <asp:Parameter Name="freguesiaIdPara" Type="Int32" />
            <asp:Parameter Name="concelhoIdPara" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="freguesiaIdDe" Type="Int32" />
            <asp:Parameter Name="concelhoIdDe" Type="Int32" />
            <asp:Parameter Name="freguesiaIdPara" Type="Int32" />
            <asp:Parameter Name="concelhoIdPara" Type="Int32" />
            <asp:Parameter Name="dist" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:Parameter Name="freguesiaIdDe" />
            <asp:Parameter Name="concelhoIdDe" />
            <asp:Parameter Name="freguesiaIdPara" />
            <asp:Parameter Name="concelhoIdPara" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="dist" Type="Int32" />
            <asp:Parameter Name="freguesiaIdDe" Type="Int32" />
            <asp:Parameter Name="concelhoIdDe" Type="Int32" />
            <asp:Parameter Name="freguesiaIdPara" Type="Int32" />
            <asp:Parameter Name="concelhoIdPara" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceConcelho" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [concelho]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceFreguesiaDe" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [freguesia] WHERE ([concelhoId] = @concelhoId)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlConcelhoDe" DefaultValue="0" Name="concelhoId"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceFreguesiaPara" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [freguesia] WHERE ([concelhoId] = @concelhoId)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlConcelhoPara" DefaultValue="0" Name="concelhoId"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
