﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

public partial class admin_pesos : System.Web.UI.Page
{
    SqlConnection connection;

    protected void Page_Load(object sender, EventArgs e)
    {
        string cnnString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        connection = new SqlConnection(cnnString);
        connection.Open();
        if (!IsPostBack)
        {
            Config cfg = new Config(connection);

            edPesos.estado = cfg.pesoEstado;
            edPesos.freguesia = cfg.pesoFreguesia;
            edPesos.tipo = cfg.pesoTipo;
            edPesos.tipologia = cfg.pesoTipologia;
            edPesos.preco = cfg.pesoPreco;
            edPesos.objectivo = cfg.pesoObjectivo;
            edPesos.area = cfg.pesoArea;
        }


    }


    protected void btGravar_Click(object sender, EventArgs e)
    {
        Config cfg = new Config(connection);
        cfg.pesoEstado = edPesos.estado;
        cfg.pesoFreguesia = edPesos.freguesia;
        cfg.pesoTipo = edPesos.tipo;
        cfg.pesoTipologia = edPesos.tipologia;
        cfg.pesoPreco = edPesos.preco;
        cfg.pesoObjectivo = edPesos.objectivo;
        cfg.pesoArea = edPesos.area;
    }
}