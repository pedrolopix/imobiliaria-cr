﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="pesosComp.ascx.cs" Inherits="comps_pesos" %>
<script type="text/javascript">
$(document).ready(function(){
  $("#sliderTipologia").slider(
  {
    value: 0<%=edTipologia.Value%>,
    min:0,
    max:10,
    slide:function(event,ui) 
    {
      $("#<%= lblTipologia.ClientID%>").text(ui.value);
      $("#<%= edTipologia.ClientID%>").val(ui.value);
    }
  });

  $("#sliderEstado").slider(
  {
    value: 0<%=edEstado.Value%>,
    min:0,
    max:10,
    slide:function(event,ui) 
    {
      $("#<%= lblEstado.ClientID%>").text(ui.value);
      $("#<%= edEstado.ClientID%>").val(ui.value);
    }
  });

    $("#sliderFreguesia").slider(
  {
    value: 0<%=edFreguesia.Value%>,
    min:0,
    max:10,
    slide:function(event,ui) 
    {
      $("#<%= lblFreguesia.ClientID%>").text(ui.value);
      $("#<%= edFreguesia.ClientID%>").val(ui.value);
    }
  });

  $("#sliderTipo").slider(
  {
    value: 0<%=edTipo.Value%>,
    min:0,
    max:10,
    slide:function(event,ui) 
    {
      $("#<%= lblTipo.ClientID%>").text(ui.value);
      $("#<%= edTipo.ClientID%>").val(ui.value);
    }
  });

    $("#sliderPreco").slider(
  {
    value: 0<%=edPreco.Value%>,
    min:0,
    max:10,
    slide:function(event,ui) 
    {
      $("#<%= lblPreco.ClientID%>").text(ui.value);
      $("#<%= edPreco.ClientID%>").val(ui.value);
    }
  });

  $("#sliderObjectivo").slider(
  {
    value: 0<%=edObjectivo.Value%>,
    min:0,
    max:10,
    slide:function(event,ui) 
    {
      $("#<%= lblObjectivo.ClientID%>").text(ui.value);
      $("#<%= edObjectivo.ClientID%>").val(ui.value);
    }
  });

    $("#sliderArea").slider(
  {
    value: 0<%=edArea.Value%>,
    min:0,
    max:10,
    slide:function(event,ui) 
    {
      $("#<%= lblArea.ClientID%>").text(ui.value);
      $("#<%= edArea.ClientID%>").val(ui.value);
    }
  });


});


</script>
<asp:Table ID="Table1" runat="server">
    <asp:TableRow>
        <asp:TableCell Width="80px">
            <asp:Label ID="Label1" runat="server" Text="Tipologia:"></asp:Label>
        </asp:TableCell>
        <asp:TableCell Width="150px">                
                <div id="sliderTipologia"></div>
        </asp:TableCell>
        <asp:TableCell Width="20px" HorizontalAlign="Right">
            <asp:Label ID="lblTipologia" runat="server"></asp:Label>
            <asp:HiddenField ID="edTipologia" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Label3" runat="server" Text="Estado:"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>                
                <div id="sliderEstado"></div>
        </asp:TableCell>
        <asp:TableCell Width="20px" HorizontalAlign="Right">
            <asp:Label ID="lblEstado" runat="server"></asp:Label>
            <asp:HiddenField ID="edEstado" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Label2" runat="server" Text="Freguesia:"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>                
                <div id="sliderFreguesia"></div>
        </asp:TableCell>
        <asp:TableCell Width="20px" HorizontalAlign="Right">
            <asp:Label ID="lblFreguesia" runat="server"></asp:Label>
            <asp:HiddenField ID="edFreguesia" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Label4" runat="server" Text="Tipo:"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>                
                <div id="sliderTipo"></div>
        </asp:TableCell>
        <asp:TableCell Width="20px" HorizontalAlign="Right">
            <asp:Label ID="lblTipo" runat="server"></asp:Label>
            <asp:HiddenField ID="edTipo" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Label5" runat="server" Text="Preço:"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>                
                <div id="sliderPreco"></div>
        </asp:TableCell>
        <asp:TableCell Width="20px" HorizontalAlign="Right">
            <asp:Label ID="lblPreco" runat="server"></asp:Label>
            <asp:HiddenField ID="edPreco" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Label6" runat="server" Text="Objectivo:"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>                
                <div id="sliderObjectivo"></div>
        </asp:TableCell>
        <asp:TableCell Width="20px" HorizontalAlign="Right">
            <asp:Label ID="lblObjectivo" runat="server"></asp:Label>
            <asp:HiddenField ID="edObjectivo" runat="server" Value="1"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Label7" runat="server" Text="Area:"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>                
                <div id="sliderArea"></div>
        </asp:TableCell>
        <asp:TableCell Width="20px" HorizontalAlign="Right">
            <asp:Label ID="lblArea" runat="server"></asp:Label>
            <asp:HiddenField ID="edArea" runat="server" Value="1"/>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
