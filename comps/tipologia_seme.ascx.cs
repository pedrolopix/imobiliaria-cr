﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class comps_tipologia_dist : System.Web.UI.UserControl
{
    int tran = 0;
    static string connString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
    SqlConnection dbConnection = new SqlConnection(connString);

    protected void Page_Load(object sender, EventArgs e)
    {
        buildGrid();
        
        
        
    }

    private void buildGrid()
    {
        DataView dview = (DataView)SqlTipologia.Select(DataSourceSelectArguments.Empty);
        TableRow row = new TableRow();
        TableCell cell = new TableCell();
        cell.Controls.Add(new LiteralControl());
        row.Cells.Add(cell);

        foreach(DataRow drow in dview.Table.Rows)
        {
            cell = new TableCell();
            cell.Controls.Add(new LiteralControl(drow["nome"].ToString()));
            row.Cells.Add(cell);
        }
        table.Rows.Add(row);
        
        foreach (DataRow dcol in dview.Table.Rows)
        {

            row = new TableRow();
            cell = new TableCell();
            row.Cells.Add(cell);
            cell.Controls.Add(new LiteralControl(dcol["nome"].ToString()));

            foreach (DataRow drow in dview.Table.Rows)
            {
                cell = new TableCell();
                TextBox tb = new TextBox();
                tb.Width = 50;
                tb.ID = "my"+drow["tipologiaId"].ToString() + "_" + dcol["tipologiaId"].ToString();
                //tb.Text = dcol["nome"].ToString() + "," + drow["nome"].ToString();
                if (!IsPostBack) tb.Text = getTipologiaValue(drow["tipologiaId"].ToString(), dcol["tipologiaId"].ToString());
                cell.Controls.Add(tb);
                cell.Controls.Add(Utils.RangeValidatorLE1(tb));
                row.Cells.Add(cell);
            }
            table.Rows.Add(row);
        }

    }

    private string getTipologiaValue(string idDe, string idPara)
    {
        if (dbConnection.State == ConnectionState.Closed)
            dbConnection.Open();

        SqlCommand cmd = dbConnection.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Get_TipologiaSeme";
        SqlParameter de=cmd.Parameters.Add("@de", SqlDbType.Int); 
        de.Direction = ParameterDirection.Input;
        de.Value = idDe;

        SqlParameter para =  cmd.Parameters.Add("@para", SqlDbType.Int);
        para.Direction = ParameterDirection.Input;
        para.Value = idPara;
        SqlParameter seme =  cmd.Parameters.Add("@result", SqlDbType.Float  );
        seme.DbType = DbType.Decimal;
        seme.Direction = ParameterDirection.ReturnValue;
        
        seme.Scale = 1;
        seme.Precision = 2;
        cmd.Parameters.Add("@retval", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

        cmd.ExecuteNonQuery();

        if (seme.Value != DBNull.Value)
            return seme.Value.ToString();

        return "0.0";
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        
        SqlTipologiaSeme.Delete();
        
        DataView dview = (DataView)SqlTipologia.Select(DataSourceSelectArguments.Empty);
        foreach (DataRow dcol in dview.Table.Rows)
        {
            foreach (DataRow drow in dview.Table.Rows)
            {
                string id = "my" + drow["tipologiaId"].ToString() + "_" + dcol["tipologiaId"].ToString();
                TextBox txtBox = (TextBox)pnlEdit.FindControl(id);
                if (txtBox != null && Convert.ToDouble(txtBox.Text)!=0)
                {
                    SqlTipologiaSeme.InsertParameters["tipologiaIdDe"].DefaultValue = drow["tipologiaId"].ToString();
                    SqlTipologiaSeme.InsertParameters["tipologiaIdPara"].DefaultValue = dcol["tipologiaId"].ToString();
                    SqlTipologiaSeme.InsertParameters["valor"].DefaultValue = txtBox.Text;
                    SqlTipologiaSeme.Insert();
                }
            }
        }
    }


    private void BeginTransation(SqlDataSourceCommandEventArgs e)
    {
        if (tran == 0)
        {
            e.Command.Connection.Open();
            e.Command.Transaction = e.Command.Connection.BeginTransaction();
        }
        tran++;
    }

    private void EndTransation(SqlDataSourceStatusEventArgs e)
    {
        tran--;
        if (tran == 0)
        {
            e.Command.Transaction.Commit();
        }

    }


}