﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class comps_pesos : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblTipologia.Text = edTipologia.Value;
        lblTipo.Text = edTipo.Value;
        lblEstado.Text = edEstado.Value;
        lblFreguesia.Text = edFreguesia.Value;
        lblPreco.Text = edPreco.Value;
        lblObjectivo.Text = edObjectivo.Value;
        lblArea.Text = edArea.Value;
    }

    public int tipologia
    {
        get { return Convert.ToInt32(edTipologia.Value); }
        set
        {
            edTipologia.Value = value.ToString();
            lblTipologia.Text = value.ToString();
        }
    }

    public int tipo
    {
        get { return Convert.ToInt32(edTipo.Value); }
        set
        {
            edTipo.Value = value.ToString();
            lblTipo.Text = value.ToString();
        }
    }

    public int estado
    {
        get { return Convert.ToInt32(edEstado.Value); }
        set
        {
            edEstado.Value = value.ToString();
            lblEstado.Text = value.ToString();
        }
    }

    public int freguesia
    {
        get { return Convert.ToInt32(edFreguesia.Value); }
        set
        {
            edFreguesia.Value = value.ToString();
            lblFreguesia.Text = value.ToString();
        }
    }

    public int preco
    {
        get { return Convert.ToInt32(edPreco.Value); }
        set
        {
            edPreco.Value = value.ToString();
            lblPreco.Text = value.ToString();
        }
    }

    public int objectivo
    {
        get { return Convert.ToInt32(edObjectivo.Value); }
        set
        {
            edObjectivo.Value = value.ToString();
            lblObjectivo.Text = value.ToString();
        }
    }

    public int area
    {
        get { return Convert.ToInt32(edArea.Value); }
        set
        {
            edArea.Value = value.ToString();
            lblArea.Text = value.ToString();
        }
    }

}

