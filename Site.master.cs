﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Roles.IsUserInRole("Admin"))
        {
            removeMenu("Admin");
        }

    }


    private void removeMenu(string menu)
    {
        List<MenuItem> list = new List<MenuItem>();
        foreach (MenuItem item in NavigationMenu.Items)
        {
            if (item.Value.Equals(menu, StringComparison.InvariantCultureIgnoreCase))
            {
                list.Add(item);
            }
        }

       while (list.Count!=0) {
           NavigationMenu.Items.Remove(list[0]);
           list.Remove(list[0]);
       }

    }

}
