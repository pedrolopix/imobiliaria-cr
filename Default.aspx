﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>


<%@ Register Src="comps/pesosComp.ascx" TagName="pesosComp" TagPrefix="uc2" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   
        <table >
            <tr> 
                <td >
                  <asp:Panel ID="Panel2" runat="server" GroupingText="Pesos" CssClass="pnlPesq">
                    <uc2:pesosComp ID="edPesos" runat="server" />
                  </asp:Panel>
                </td>
                <td valign="top">
                 <asp:Panel ID="PanelPesq" runat="server" GroupingText="Pesquisa" CssClass="pnlPesq">
                    <table>
                        <tr>
                            <td>
                                <asp:Literal ID="Literal1" runat="server" Text="Tipologia:"></asp:Literal>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTipologia" runat="server" DataSourceID="SqlDataSourceTipologia"
                                    DataTextField="nome" DataValueField="tipologiaId" CssClass="edPesq">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Literal ID="Literal2" runat="server" Text="Objetivo:"></asp:Literal>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlObjetivo" runat="server" DataSourceID="SqlDataSourceObjetivo"
                                    DataTextField="nome" DataValueField="objectivoId" CssClass="edPesq">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Literal ID="Literal3" runat="server" Text="Estado:"></asp:Literal>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlEstado" runat="server" DataSourceID="SqlDataSourceEstado"
                                    DataTextField="nome" DataValueField="EstadoId" CssClass="edPesq">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="Literal4" runat="server" Text="Concelho:"></asp:Literal>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlConcelho" runat="server" DataSourceID="SqlDataSourceConcelho"
                                    DataTextField="nome" DataValueField="ConcelhoId" AutoPostBack="true" CssClass="edPesq">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Literal ID="Literal5" runat="server" Text="Freguesia:"></asp:Literal>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlFreguesia" runat="server" DataSourceID="SqlDataSourceFreguesia"
                                    DataTextField="nome" DataValueField="freguesiaId" CssClass="edPesq">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Literal ID="Literal8" runat="server" Text="Tipo:"></asp:Literal>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTipo" runat="server" DataSourceID="SqlDataSourceTipo" DataTextField="nome"
                                    DataValueField="TipoId" CssClass="edPesq">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="Literal6" runat="server" Text="Limite de preço:"></asp:Literal>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPrecoDe" runat="server" CssClass="edPesq">0</asp:TextBox>
                            </td>
                            <td>
                                <asp:Literal ID="Literal9" runat="server" Text="Área:"></asp:Literal>
                            </td>
                            <td>
                                <!--
                                <input id="cbkPesos" type="checkbox" onchange="javaScript:hideDiv(this);" runat="server" /><asp:Label
                                    ID="Label1" runat="server" Text="Definir pesos"></asp:Label> -->
                                <asp:TextBox ID="txtArea" runat="server" CssClass="edPesq">0</asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        <tr>
                        <td></td>
                         <td>
                             <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                 ControlToValidate="txtPrecoDe" CssClass="validator" ErrorMessage="Só números!" 
                                 MaximumValue="400000000" MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
                            </td>
                          <td></td>
                           <td>
                               <asp:RangeValidator ID="RangeValidator2" runat="server" 
                                   ControlToValidate="txtArea" CssClass="validator" ErrorMessage="Só números!" 
                                   MaximumValue="400000000" MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
                            </td>
                            <td></td>
                             <td></td>
                              
                        </tr>
                        <tr><td colspan="6" align="right"><asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" 
                                    onclick="btPesquisar_Click" /></td></tr>
                        
                    </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    
    <asp:Panel ID="Panel1" runat="server">
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" DataSourceID="SqlDataSourceImobi" ForeColor="#333333" 
            GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
              
                <asp:BoundField DataField="Seme" HeaderText="Seme" SortExpression="Seme"  />
                <asp:BoundField DataField="imovelId" HeaderText="imovelId" SortExpression="imovelId" />
                <asp:HyperLinkField DataNavigateUrlFields="ImovelId" 
                    DataNavigateUrlFormatString="~/manutencao/novo_imovel.aspx?id={0}" DataTextField="titulo" 
                    HeaderText="Titulo" /> 
                
                <asp:BoundField DataField="titulo" HeaderText="Título" SortExpression="titulo" />
                <asp:BoundField DataField="tipologia" HeaderText="Tipologia" SortExpression="tipologia" />
                <asp:BoundField DataField="objectivo" HeaderText="Objetivo" SortExpression="objectivo" />
                <asp:BoundField DataField="estado" HeaderText="Estado" SortExpression="estado" />
                
                
                
                <asp:BoundField DataField="concelho" HeaderText="Concelho" SortExpression="concelho" />
                <asp:BoundField DataField="freguesia" HeaderText="Freguesia" SortExpression="freguesia" />

                
                <asp:BoundField DataField="tipo" HeaderText="Tipo" SortExpression="tipo" />
                <asp:BoundField DataField="area" HeaderText="Área" SortExpression="area" />

                <asp:BoundField DataField="preco" HeaderText="Preço" SortExpression="preco" ItemStyle-HorizontalAlign="Right"
                    ItemStyle-Wrap="false" DataFormatString="{0:N}" >
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
                </asp:BoundField>

            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#4b6b9e" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#4b6b9e" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#4b6b9e" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </asp:Panel>
    <asp:SqlDataSource ID="SqlDataSourceTipologia" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [tipologia] WHERE nome !='' order by nome"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceImobi" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand='SELECT dbo.ImobiSeme(imovel.EstadoId,@EstadoId,@EstadoPeso,imovel.concelhoId,imovel.FreguesiaId,@ConcelhoId,@FreguesiaId,@FreguesiaPeso,imovel.tipologiaId,@TipologiaId,@TipologiaPeso,imovel.tipoId,@TipoId,@TipoPeso,imovel.ObjectivoId,@Objectivo,@ObjectivoPeso,imovel.preco,@Preco,@PrecoPeso,imovel.AreaUtil,@area,@AreaPeso) as Seme ,imovel.imovelId, imovel.titulo, imovel.ref, imovel.localidade, imovel.codPostal, imovel.morada, imovel.anoConstrucao, imovel.nwcs, imovel.nquartos, freguesia.nome as freguesia, tipologia.nome AS tipologia, imovel.descricao, imovel.areaBruta, imovel.areaTerreno, imovel.zona, imovel.preco, imovel.areaUtil as area, distrito.nome AS distrito, estado.nome AS estado, objectivo.nome AS objectivo, tipo.nome AS tipo, concelho.nome AS concelho FROM concelho INNER JOIN imovel ON concelho.concelhoId = imovel.concelhoId INNER JOIN objectivo ON imovel.objectivoId = objectivo.objectivoId INNER JOIN tipo ON imovel.tipoId = tipo.tipoId INNER JOIN tipologia ON imovel.tipologiaId = tipologia.tipologiaId INNER JOIN estado ON imovel.estadoId = estado.estadoId INNER JOIN freguesia ON imovel.concelhoId = freguesia.concelhoId AND imovel.freguesiaId = freguesia.freguesiaId CROSS JOIN distrito where  preco<=@preco   ORDER BY dbo.ImobiSeme(imovel.EstadoId,@EstadoId,@EstadoPeso,imovel.concelhoId,imovel.FreguesiaId,@ConcelhoId,@FreguesiaId,@FreguesiaPeso,imovel.tipologiaId,@TipologiaId,@TipologiaPeso,imovel.tipoId,@TipoId,@TipoPeso,imovel.ObjectivoId,@Objectivo,@ObjectivoPeso,imovel.preco,@Preco,@PrecoPeso,imovel.AreaUtil,@area,@AreaPeso) desc, preco asc, area desc'
        OnSelecting="SqlDataSourceImobi_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlEstado" Name="EstadoId" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="edPesos" DefaultValue="" Name="EstadoPeso" PropertyName="estado" />
            <asp:ControlParameter ControlID="ddlConcelho" Name="ConcelhoId" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="ddlFreguesia" Name="FreguesiaId" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="edPesos" DefaultValue="" Name="FreguesiaPeso" PropertyName="freguesia" />
            <asp:ControlParameter ControlID="ddlTipologia" Name="TipologiaId" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="edPesos" DefaultValue="" Name="TipologiaPeso" PropertyName="tipologia" />
            <asp:ControlParameter ControlID="ddlTipo" Name="TipoId" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="edPesos" DefaultValue="" Name="TipoPeso" PropertyName="tipo" />
            <asp:ControlParameter ControlID="ddlObjetivo" Name="Objectivo" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="edPesos" DefaultValue="" Name="ObjectivoPeso" PropertyName="objectivo" />
            <asp:ControlParameter ControlID="txtPrecoDe" Name="Preco" PropertyName="Text" />
            <asp:ControlParameter ControlID="edPesos" DefaultValue="" Name="PrecoPeso" PropertyName="preco" />
            <asp:ControlParameter ControlID="txtArea" Name="area" PropertyName="Text" />
            <asp:ControlParameter ControlID="edPesos" Name="AreaPeso" PropertyName="Area" />
            
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceObjetivo" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [objectivo] where nome !=''"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceEstado" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [estado] where nome !=''"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceConcelho" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [concelho]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceFreguesia" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [freguesia] WHERE ([concelhoId] = @concelhoId)"
        OnSelecting="SqlDataSourceFreguesia_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlConcelho" DefaultValue="0" Name="concelhoId"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceTipo" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
        SelectCommand="SELECT * FROM [tipo]"></asp:SqlDataSource>
</asp:Content>
