﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class _Default : System.Web.UI.Page
{

    SqlConnection connection;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string cnnString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            connection = new SqlConnection(cnnString);
            connection.Open();
            Config cfg = new Config(connection);

            edPesos.estado= cfg.pesoEstado;
            edPesos.freguesia = cfg.pesoFreguesia;
            edPesos.tipo=  cfg.pesoTipo;
            edPesos.tipologia = cfg.pesoTipologia;

        }

        //SqlDataSourceImobi.SelectParameters["TipoPeso"].DefaultValue = Convert.ToString(edPesos.tipo);
        //SqlDataSourceImobi.SelectParameters["FreguesiaPeso"].DefaultValue = edPesos.freguesia.ToString();
        //SqlDataSourceImobi.SelectParameters["EstadoPeso"].DefaultValue = edPesos.estado.ToString();
        //SqlDataSourceImobi.SelectParameters["TipologiaPeso"].DefaultValue = edPesos.tipologia.ToString();


    }


    protected void SqlDataSourceFreguesia_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSourceImobi_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
}
