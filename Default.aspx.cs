﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;

public partial class _Default : System.Web.UI.Page
{

    SqlConnection connection;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string cnnString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            connection = new SqlConnection(cnnString);
            connection.Open();
            Config cfg = new Config(connection);

            edPesos.estado= cfg.pesoEstado;
            edPesos.freguesia = cfg.pesoFreguesia;
            edPesos.tipo=  cfg.pesoTipo;
            edPesos.tipologia = cfg.pesoTipologia;
            edPesos.preco = cfg.pesoPreco;
            edPesos.objectivo = cfg.pesoObjectivo;
            edPesos.area = cfg.pesoArea;


        }

        bool ia= isAdmin();

        setVisibility("Seme", ia);
        setVisibility("imovelId", ia);
        setVisibility("Titulo", ia);
        setVisibility("Título", !ia);
       
        
    }

    private void setVisibility(string coluna,bool visible=false)
    {
        ((DataControlField)GridView1.Columns
               .Cast<DataControlField>()
               .Where(fld => (fld.HeaderText == coluna))
               .SingleOrDefault()).Visible = visible;
    }

    public bool isAdmin()
    {
        return Roles.IsUserInRole("Admin");
    }


    protected void SqlDataSourceFreguesia_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void SqlDataSourceImobi_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        //par fazer post back
    }
}
